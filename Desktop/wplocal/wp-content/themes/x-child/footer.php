<?php

// =============================================================================
// FOOTER.PHP
// -----------------------------------------------------------------------------
// The site footer. Consists of the top footer area for widgets and the bottom
// footer area for secondary information. Includes wp_footer() hook as well.
//
// Content is output based on which Stack has been selected in the Customizer.
// To view and/or edit the markup of your Stack's index, first go to "views"
// inside the "framework" subdirectory. Once inside, find your Stack's folder
// and look for a file called "wp-footer.php," where you'll be able to find the
// appropriate output.
// =============================================================================

?>


 
<footer class="footerWrap">
        <div class="footer1"><img src=
        "http://writespikesite.wpengine.com/wp-content/uploads/2016/05/logo-1.png" alt="" class="logo">
        <img src=
        "http://writespikesite.wpengine.com/wp-content/uploads/2016/05/WRITE.-SHARE.-DISCOV.png" alt="" class="write" >
        </div>
        <hr>
        <div class="footer2">
            <p class="footerMid1">Copyright WriteSpike 2016</p>
            <p class="footerMid2"><a href="https://www.facebook.com/"><img alt=
            "" class="socialMedia" src=
            "http://writespikesite.wpengine.com/wp-content/uploads/2016/06/fb.png"></a>
            <a href="https://twitter.com/"><img alt="" class="socialMedia" src=
            "http://writespikesite.wpengine.com/wp-content/uploads/2016/06/twitter.png">
            </a> <a href=
            "https://plus.google.com/collections/featured"><img alt="" class=
            "socialMedia" src=
            "http://writespikesite.wpengine.com/wp-content/uploads/2016/06/google.png">
            </a></p>
            <p class="footerMid3"><a class="footerLink" href=
            "http://writespikesite.wpengine.com/privacy-policy/">Privacy
            Policy</a> <a class="footerLink" href=
            "http://writespikesite.wpengine.com/help/">Help</a> <a class=
            "footerLink" href=
            "http://writespikesite.wpengine.com/contact-us/">Contact Us</a></p>
        </div>
</footer>

<?php x_get_view( x_get_stack(), 'wp', 'footer' ); ?>
