<?php

	$css_selector = '#circles_content_'.$circle_content->id;
	$width_circle = $height_circle = $circle_content->icon_size+round($circle_content->icon_size/5);
	$padding_circle = round($circle_content->icon_size/5);
	
?> 
	<style>

		<?= $css_selector ?> .title {
			font-size: <?= $circle_content->title_size ?>px;
		}

		<?= $css_selector ?> .title span {
			width: <?= $circle_content->width-2*$width_circle-4*$padding_circle ?>px;
		}

		<?= $css_selector ?> .circle  {
			font-size: <?= $circle_content->icon_size ?>px;
			padding: <?= $padding_circle ?>px;
			width: <?= $width_circle ?>px;
			height: <?= $height_circle ?>px;
			line-height: <?= $height_circle ?>px;
		}

	</style>
	
		<div id="circles_content_<?= $circle_content->id ?>" class="circles_content" style="width: <?= $circle_content->width ?>px; height:<?= $circle_content->height ?>px">
			<?php

				foreach($circle_content->icons as $icon)
				{
					echo '<a href="'.$icon->link.'" '.($icon->blank == 1 ? 'target="_blank"' : '').' class="circle" rel="'.nl2br($icon->name).'" style="color: '.$icon->color_icon.'; background: '.$icon->color_bg.'"><i class="fa fa-'.$icon->icon.'"></i></a>';
				}
			?>
			<div class="title" style="line-height: <?= $circle_content->height ?>px; color: <?= $circle_content->title_color ?>"><span></span></div>
		</div>
</body>
</html>