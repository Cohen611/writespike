To translate strings from the grid you should use .po/.mo files located in the-grid/langs/ folder.
You also need to setup wordpress language if it's not already the case:https://make.wordpress.org/polyglots/handbook/

.po/.mo files must be named like this (prefixed by "tg-text-domain"):

	- tg-text-domain-fr_FR.po
	- tg-text-domain-fr_FR.mo

And you can easly edit these files with Poedit software: https://poedit.net/

You can aslo use a 3rd party plugin to easly translate strings like WPML plugin. The Grid is compatible with WPML.

Themeone Team